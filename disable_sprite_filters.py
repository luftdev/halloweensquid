#!/usr/bin/env python
import os

# changes all import files to disable sprite filtering


# config
import_dir = ".import/"
sprite_dir = "./texture/"


# vars
import_files = []


# clear .import folder
for filename in os.listdir(import_dir):
  os.remove(import_dir + filename)


# get directories to import files
for filename in os.listdir(sprite_dir):
  if("import" in filename):
    import_files.append(filename)


# open each file and modify the line
for filename in import_files:
  # store the file in memory
  lines = []
  path = sprite_dir + filename
  
  # read
  f = open(path, "r")
  for line in f:
    lines.append(line)
  f.close()
  
  # write and modify
  f = open(path, "w")
  for line in lines:
    if(line == "flags/filter=true\n"):
      f.write("flags/filter=false\n")
    else:
      f.write(line)
  f.close()
