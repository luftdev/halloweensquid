# behavior tree pseudocode notes

class Node:
  status = {
    IDLE
    PROCESSING
    SUCCESS
    FAILURE
  }
  
  child
  num_children


class Selector: Node
  
  tick():
    child_status = child.tick()
    
    if(child_status == status.FAILURE):
      child++
      if(child > num_children):
        child = first_child
        return status.FAILURE
    
    if(child_status == status.SUCCESS):
        child = first_child
        return status.SUCCESS
    
    return child_status


class Sequence: Node
  
  tick():
    child_status = child.tick()
    
    if(child_status == status.FAILURE):
      child = first_child
      return status.FAILURE
    
    if(child_status == status.SUCCESS):
      child++
    
    if(child > num_children):
      child = first_child
      return status.SUCCESS
    
    return status.PROCESSING
    
    
    
    
    
    
