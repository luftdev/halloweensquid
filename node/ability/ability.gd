extends Node


# class
class_name Ability


# ref
var actor : Actor
var timer : Timer


# signal
signal state_change


# var
var state : int = 0


# init
func _ready():
	actor = get_parent()
	timer = get_node("timer")
	
	timer.connect("timeout", self, "on_timeout")


# handle timer event
func on_timeout():
	pass


# perform a state change, which notifies other nodes
func change_state(next_state):
	state = next_state
	emit_signal("state_change", state)
