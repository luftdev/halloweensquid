extends KinematicBody2D


# class
class_name Actor


# config
export var ground_friction : float = 100
export var air_friction : float = 10
export var gravity : float = 300


# ref
var cs2d : CollisionShape2D
var sprite : Sprite
var animator : AnimationPlayer


# var
var gravity_enabled : bool = true
var vel : Vector2
var ceiling : bool = false
var grounded : bool = false
var wall_left : bool = false
var wall_right : bool = false


# signal
signal land
signal collision
signal slide_off


# init
func _ready():
	cs2d = get_node("cs2d")
	sprite = get_node("sprite")
	animator = get_node("animator")



# face sprite in a direction
func face_towards(bearing):
	if(bearing.x != 0):
		sprite.flip_h = bearing.x > 0


# physics
func _physics_process(delta):
	# gravity
	if(gravity_enabled):
		push(Vector2.DOWN * gravity, delta)
	
	# perform move
	move_and_slide(vel, Vector2.UP)
	
	# reset contact booleans
	var new_ceiling = false
	var new_grounded = false
	var new_wall_left = false
	var new_wall_right = false
	
	# zero out velocity against collision normals
	for i in range(get_slide_count()):
		var collision = get_slide_collision(i)
		var normal = collision.normal
		var normal_right = Vector2(normal.y, -normal.x)
		vel = vel.project(normal_right)
		
		new_ceiling = new_ceiling or normal.y > 0
		new_grounded = new_grounded or normal.y < 0
		new_wall_left = new_wall_left or normal.x > 0
		new_wall_right = new_wall_right or normal.x < 0
		
		if(new_grounded and not grounded):
			emit_signal("land")
	
	if(not new_grounded and grounded):
		emit_signal("slide_off")
	
	if(get_slide_count() > 0):
		emit_signal("collision")
	
	ceiling = new_ceiling
	grounded = new_grounded
	wall_left = new_wall_left
	wall_right = new_wall_right
	
	# calculate x friction
	apply_friction(delta)


# enable actor collision
func enable_collision():
	cs2d.disabled = false


# enable actor collision
func disable_collision():
	cs2d.disabled = true


# enable gravity's affect on this actor
func enable_gravity():
	gravity_enabled = true


# disable gravity's affect on this actor
func disable_gravity():
	gravity_enabled = false


# add a velocity impulse
func add_vel(delta_vel):
	vel += delta_vel


# set velocity to a specific value
func set_vel(new_vel):
	vel = new_vel


# apply force (requires delta)
func push(push_acc, delta):
	vel += push_acc * delta


# apply a force with a speed cap (requires delta)
func push_until(push_acc, until_vel, delta):
	var new_vel = vel + push_acc * delta
	if(until_vel.dot(new_vel) <= 0):
		vel = new_vel
		return
	
	var vel_prj = vel.project(until_vel)
	var new_prj = new_vel.project(until_vel)
	
	if(vel_prj.length() > until_vel.length()):
		return
	
	if(new_prj.length() > until_vel.length()):
		vel = new_vel + (until_vel - new_prj)
	else:
		vel = new_vel


# perform friction
func apply_friction(delta):
	var friction = air_friction
	
	if(grounded):
		friction = ground_friction
	
	var new_vel = vel - Vector2.RIGHT * sign(vel.x) * friction * delta
	
	if(new_vel.x * vel.x < 0):
		new_vel.x = 0
	
	vel = new_vel
