extends Area2D


# class
class_name ActorPush


# config
export var force : float = 200


# ref
var actor : Actor


# var
var enabled : bool = true


# init
func _ready():
	actor = get_parent()


# enable soft collisions
func enable():
	enabled = true


# disable soft collisions
func disable():
	enabled = false


# physics
func _physics_process(delta):
	if(not enabled):
		return
	
	var areas = get_overlapping_areas()
	var push_force = Vector2.ZERO
	
	for area in areas:
		if(not area.enabled):
			continue
		
		var displacement = global_position - area.global_position
		var distance = displacement.length()
		var bearing = displacement.normalized()
		push_force += bearing / (distance + 1)
	
	push_force *= force / max(1, len(areas))
	actor.push(push_force, delta)
