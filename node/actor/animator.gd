extends AnimationPlayer


# class
class_name Animator


# config
export var state_machine_name : String = "state_machine"


# ref
var state_machine : StateMachine


# var
var state_animations : Dictionary


# init
func _ready():
	if(get_parent().has_node(state_machine_name)):
		state_machine = get_parent().get_node(state_machine_name)
	
	if(state_machine):
		state_machine.connect("state_change", self, "on_state_change")
	
	for state_animation in get_children():
		state_animations[state_animation.name] = state_animation.animation_name
		state_animation.queue_free()


# react to a new state
func on_state_change(state_name):
	var state_animation = state_animations.get(state_name)
	
	if(state_animation):
		play(state_animation)



