extends Node


# class
class_name Tag, "res://node/actor/icon_tag.png"


# config
export var tags : PoolStringArray


# init
func _ready():
	for tag in tags:
		get_parent().add_to_group(tag)
