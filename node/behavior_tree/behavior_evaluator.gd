extends Node


# class
class_name BehaviorEvaluator, "res://node/behavior_tree/icon_evaluator.png"


# config
export var behavior_tree_name : String


# ref
var behavior_tree : BehaviorNode


# var
var evaluator_state : EvaluatorState


# init
func _ready():
	evaluator_state = EvaluatorState.new()
	evaluator_state.controller = get_parent()
	set_behavior_tree(behavior_tree_name)


# set behavior tree
func set_behavior_tree(behavior_tree_name):
	behavior_tree = BehaviorTreeManager.get_behavior_tree(behavior_tree_name)
	evaluator_state.prev = behavior_tree
	evaluator_state.next = behavior_tree


# tick on physics
func _physics_process(delta):
	if(evaluator_state.next):
		evaluator_state.finished_tick = false
		
		while(not evaluator_state.finished_tick):
			evaluator_state.next.tick(evaluator_state)
