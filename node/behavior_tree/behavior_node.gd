extends Node


# class
class_name BehaviorNode


# node state
enum status {
	RUNNING
	SUCCESS
	FAILURE
}


# set status to succeed and finish
func succeed(evaluator_state):
	evaluator_state.prev_status = status.SUCCESS
	finish(evaluator_state)


func fail(evaluator_state):
	evaluator_state.prev_status = status.FAILURE
	finish(evaluator_state)


# finish current node and move up
func finish(evaluator_state):
	if(get_parent() != BehaviorTreeManager):
		traverse_to(evaluator_state, get_parent())
	else:
		evaluator_state.prev = self
		evaluator_state.next = self
		evaluator_state.prev_status = status.RUNNING
		evaluator_state.finished_tick = true


# set the current node to a child
func traverse_to(evaluator_state, node):
	
	evaluator_state.prev = self
	evaluator_state.next = node


# evaluate on tick and return status
func tick(evaluator_state):
	print(name, " ticked")
	succeed(evaluator_state)
