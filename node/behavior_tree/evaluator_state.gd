extends Reference


# class
class_name EvaluatorState


# ref
var prev : BehaviorNode
var next : BehaviorNode
var controller : ActorController


# var
var prev_status : int = BehaviorNode.status.RUNNING
var finished_tick : bool = false
