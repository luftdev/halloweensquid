extends BehaviorNode


# class
class_name Executor, "res://node/behavior_tree/icon_executor.png"


# config
export var break_point : bool = false
export var function : String = "func_name"
export var args : Array


func tick(evaluator_state):
	if(not evaluator_state.controller.has_method(function)):
		breakpoint
	
	if(break_point):
		breakpoint
	
	var execution_status = evaluator_state.controller.callv(function, args)
	
	evaluator_state.finished_tick = true
	
	match(execution_status):
		
		status.SUCCESS:
			succeed(evaluator_state)
			return
		
		status.RUNNING:
			evaluator_state.prev_status = status.RUNNING
			return
		
		status.FAILURE:
			fail(evaluator_state)
			return
	
	succeed(evaluator_state)
