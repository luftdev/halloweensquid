extends BehaviorNode


# class
class_name Inverter, "res://node/behavior_tree/icon_inverter.png"


func tick(evaluator_state):
	
	match(evaluator_state.prev_status):
		
		status.RUNNING:
			evaluator_state.prev_status = status.RUNNING
			var child = get_child(0)
			traverse_to(evaluator_state, child)
		
		status.SUCCESS:
			fail(evaluator_state)
		
		status.FAILURE:
			succeed(evaluator_state)
