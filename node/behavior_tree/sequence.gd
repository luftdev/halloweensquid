extends BehaviorNode


# class
class_name Sequence, "res://node/behavior_tree/icon_sequence.png"


func tick(evaluator_state):
	var child_id = 0
	
	if(evaluator_state.prev.get_parent() == self):
		child_id = evaluator_state.prev.get_index()
	
	var child = get_child(child_id)
	
	match(evaluator_state.prev_status):
		
		status.RUNNING:
			evaluator_state.prev_status = status.RUNNING
			traverse_to(evaluator_state, child)
		
		status.SUCCESS:
			child_id += 1
			
			if(child_id >= get_child_count()):
				succeed(evaluator_state)
			else:
				evaluator_state.prev_status = status.RUNNING
				child = get_child(child_id)
				traverse_to(evaluator_state, child)
		
		status.FAILURE:
			fail(evaluator_state)
