extends Camera2D


# class
class_name CameraFollow


# config
export var weight : float = 0.20


# ref
var target_ref : SafeRef
var true_position : Vector2


# init
func _ready():
	target_ref = SafeRef.new(get_tree().get_nodes_in_group("player").front())
	
	# initial teleport
	if(target_ref.has_ref()):
		true_position = target_ref.get_ref().global_position
		global_position = true_position


# frame
func _physics_process(delta):
	if(target_ref.has_ref()):
		
		# do math on floatint point location to prevent jitter
		var target_position = target_ref.get_ref().global_position
		true_position = lerp(true_position, target_position, weight)
		
		# snap draw position to make sure sprites render properly
		global_position = true_position.snapped(Vector2(1, 1))
