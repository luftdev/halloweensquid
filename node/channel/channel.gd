extends Reference


# class
class_name Channel


# signal
signal channel_write
signal channel_change


# var
var value : bool = false


# write a new value to this channel
func write(new_value : bool):
	if(value != new_value):
		emit_signal("channel_change", new_value)
	
	emit_signal("channel_write", new_value)
	
	value = new_value
