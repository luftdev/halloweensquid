extends Node


# ref
var actor : Actor
var timer : Timer


# var
var color : Color = Color.black
var is_blinking : bool = false
var delay : float = 0.05


# init
func _ready():
	actor = get_parent()
	timer = get_node("timer")
	
	timer.connect("timeout", self,  "on_timeout")


# enable blinking
func blink(new_color, new_delay = delay):
	if(not is_blinking):
		is_blinking = true
		color = new_color
		delay = new_delay
		timer.start(delay)


# disable blinking
func stop():
	if(is_blinking):
		is_blinking = false
		actor.sprite.modulate = Color.white


# perform on timeout
func on_timeout():
	if(is_blinking):
		if(actor.sprite.modulate == color):
			actor.sprite.modulate = Color.white
		else:
			actor.sprite.modulate = color
		
		timer.start(delay)
