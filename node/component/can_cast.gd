extends Ability


# class
class_name CanCast


# magic_types
enum magic_types {
	GREEN_MAGIC
	RED_MAGIC
}


# config
export(magic_types) var magic_type
export var charge_time : float = 0.75
export var cast_time : float = 0.50
export var cooldown_time : float = 1.0
export var offset : Vector2
export var bearing_offset : float = 10


# states
enum states {
	IDLE
	CHARGING
	READY
	CASTING
	COOLDOWN
}


# ref
var last_magic_ball : SafeRef


# when charge timer is finished
func on_timeout():
	match(state):
		
		states.CHARGING:
			change_state(states.READY)
	
		states.CASTING:
			change_state(states.COOLDOWN)
			timer.start(cooldown_time)
	
		states.COOLDOWN:
			change_state(states.IDLE)


# initiates a spit attack
func charge_cast():
	if(state == states.IDLE):
		change_state(states.CHARGING)
		timer.start(charge_time)


# perform magic cast
func cast(bearing):
	if(state == states.READY):
		change_state(states.CASTING)
		timer.start(cast_time)
		
		bearing = (bearing * Vector2.RIGHT).normalized()
		
		var projectile_asset_path
		
		match magic_type:
			
			magic_types.GREEN_MAGIC:
				projectile_asset_path = "projectile/green_magic_ball"
			
			magic_types.RED_MAGIC:
				projectile_asset_path = "projectile/red_magic_ball"
		
		var magic_ball = Creator.create_at_position(projectile_asset_path, actor.global_position + offset + bearing * bearing_offset)
		
		magic_ball.launch(bearing)
		
		last_magic_ball = SafeRef.new(magic_ball)


# determine if the actor can cast
func can_cast():
	if(last_magic_ball and last_magic_ball.has_ref()):
		return false
	return state == states.IDLE and actor.grounded
