extends Node


# class
class_name CanChangeMagicType


# ref
var projectile : Projectile
var hurtbox : Hitbox


# init
func _ready():
	projectile = get_parent()
	hurtbox = projectile.get_node("hurtbox")
	
	hurtbox.connect("hitbox_entered", self, "on_hitbox_entered")


# check torch collision
func on_hitbox_entered(hitbox):
	match hitbox.tag:
		
		Hitbox.tags.GREEN_TORCH:
			projectile.hitbox.change_tag(Hitbox.tags.GREEN_MAGIC)
			projectile.animator.play("green")
		
		Hitbox.tags.RED_TORCH:
			projectile.hitbox.change_tag(Hitbox.tags.RED_MAGIC)
			projectile.animator.play("red")


