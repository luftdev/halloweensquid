extends Hitbox


# class
class_name CanCollect


# config
export(Array, Hitbox.tags) var pickups


# init
func _ready():
	connect("hitbox_entered", self, "on_hitbox_entered")


# handle hitbox events
func on_hitbox_entered(hitbox):
	if(not hitbox.tag in pickups):
		return
	
	var pickup : Pickup = hitbox.get_parent()
	
	pickup.attempt_pickup(get_parent())
