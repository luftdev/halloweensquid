extends Node


# class
class_name CanDropOnDeath


# config
export var item_paths : PoolStringArray
export var toss_velocity : Vector2 = Vector2(0, -100)
export var toss_velocity_variance : Vector2 = Vector2(50, 50)


# ref
var has_health : HasHealth


# init
func _ready():
	has_health = get_parent().get_node("has_health")
	
	has_health.connect("death", self, "on_death")


# drop items on death
func on_death():
	var actor = get_parent()
	for item_path in item_paths:
		var item = Creator.create_at_position(item_path, actor.global_position)
		var rand_velocity = toss_velocity_variance * Vector2(rand_range(-1, 1), rand_range(-1, 1))
		item.set_vel(toss_velocity + rand_velocity)
