extends Node


# class
class_name CanFinishLevel


# ref
var hurtbox : Hitbox


# init
func _ready():
	hurtbox = get_parent().get_node("hurtbox")
	
	hurtbox.connect("hitbox_entered", self, "on_hitbox_entered")


# handle goal
func on_hitbox_entered(hitbox):
	if(hitbox.tag == Hitbox.tags.GOAL):
		var goal : Goal = hitbox.get_parent()
		GameManager.load_scene(goal.next_level)
