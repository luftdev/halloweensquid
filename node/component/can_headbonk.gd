extends Ability


# class
class_name CanHeadbonk


# config
export var charge_time : float = 0.50
export var cooldown_time : float = 1.00


# ref
var can_move : CanMove
var can_jump : CanJump


# states
enum states {
	IDLE
	CHARGING
	READY
	HEADBONKING
	COOLDOWN
}


# init
func _ready():
	can_move = actor.get_node("can_move")
	can_jump = actor.get_node("can_jump")
	
	actor.connect("land", self, "on_land")


# executes when landing on the ground after a headbonk
func on_land():
	if(state == states.HEADBONKING):
		change_state(states.COOLDOWN)
		actor.sprite.rotation = 0
		timer.start(cooldown_time)


# when charge timer is finished
func on_timeout():
	if(state == states.CHARGING):
		change_state(states.READY)
	elif(state == states.COOLDOWN):
		change_state(states.IDLE)


# initiates a headbonk
func charge_headbonk():
	if(state == states.IDLE):
		change_state(states.CHARGING)
		timer.start(charge_time)


# enacts a headbonk, which can only happen when it's ready
func headbonk(bearing):
	if(state == states.READY):
		change_state(states.HEADBONKING)
		var direction = (bearing * Vector2.RIGHT).normalized()
		can_move.move(direction)
		can_jump.jump()
		
		actor.face_towards(direction)
		
		if(direction.x > 0):
			actor.sprite.rotation = PI * 0.5
		else:
			actor.sprite.rotation = -PI * 0.5


# determine if the actor can headbonk
func can_headbonk():
	return state == states.IDLE and actor.grounded
