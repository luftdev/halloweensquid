extends Ability


# class
class_name CanInkshot


# config
export var duration : float = 0.15
export var acceleration : float = 1000
export var init_speed : float = 60
export var init_lift : float = 30
export var max_speed : float = 120
export var anti_gravity : float = 200
export var center : Vector2
export var offset : float = 6
export var ink_angle_variance : float = 5


# states
enum states {
	IDLE
	INKSHOTTING
}


# ref
var ticker : Ticker


# var
var bearing : Vector2 = Vector2.UP


# init
func _ready():
	ticker = get_node("ticker")
	
	ticker.connect("tick", self, "on_tick")


# physics
func _physics_process(delta):
	if(state == states.INKSHOTTING):
		actor.push_until(bearing * acceleration, bearing * max_speed, delta)
		actor.push(Vector2.UP * anti_gravity, delta)


# on tick fire ink
func on_tick():
	if(state == states.INKSHOTTING):
		var shot = Creator.create_at_position("projectile/inkshot", actor.global_position + center + -bearing * offset)
		var angle = deg2rad(rand_range(-1, 1) * ink_angle_variance)
		shot.launch((bearing * -1).rotated(angle))


# begin an inkshot, which commits to a timer
func inkshot(new_bearing):
	if(state != states.IDLE):
		return
	
	change_state(states.INKSHOTTING)
	
	bearing = new_bearing
	
	actor.set_vel(actor.vel * Vector2.RIGHT)
	
	if(bearing != Vector2.DOWN):
		actor.add_vel(init_lift * Vector2.UP)
	
	timer.start(duration)


# converts a Vector2 into a proper inkshot Vector2
func set_bearing(new_bearing):
	if(abs(new_bearing.x) > abs(new_bearing.y)):
		new_bearing = (Vector2.RIGHT * new_bearing).normalized()
	else:
		new_bearing = (Vector2.DOWN * new_bearing).normalized()
	
	if(new_bearing == Vector2.ZERO):
		return bearing
	
	bearing = new_bearing
	return bearing


# determine whether or not an inkshot is possible/ready
func can_inkshot():
	return state == states.IDLE


# when the inkshot is finished, notify others
func on_timeout():
	actor.enable_gravity()
	timer.stop()
	bearing = Vector2.UP
	change_state(states.IDLE)
