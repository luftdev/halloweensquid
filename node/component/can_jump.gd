extends Node


# class
class_name CanJump


# config
export var jump_speed : float = 120
export var decay : float = 0.8


# ref
var actor : Actor


# var
var is_jumping : bool = false
var is_shortening : bool = false


# signal
signal peak


# init
func _ready():
	actor = get_parent()


# physics
func _physics_process(delta):
	if(actor.vel.y > 0 and actor.grounded):
		is_jumping = false
		is_shortening = false
	
	if(is_jumping && actor.vel.y >= 0):
		is_jumping = false
		is_shortening = false
		emit_signal("peak")
	elif(is_shortening and is_jumping):
		actor.vel = actor.vel * Vector2(1, decay)


# perform a jump
func jump():
	if(actor.grounded):
		actor.set_vel(actor.vel * Vector2.RIGHT + jump_speed * Vector2.UP)
		is_jumping = true


# begin shortening
func shorten():
	if(not actor.grounded):
		is_shortening = true
