extends Node


# class
class_name CanLocate


# config
export var locate_by_tag : String = "player"
export var exclude_self : bool = true
export var detect_range : float = 50
export var ignore_range : float = 200
export var tick_interval : float = 0.20


# ref
var actor : Actor
var timer : Timer


# signal
signal target_found
signal target_lost


# var
var target : SafeRef


# init
func _ready():
	actor = get_parent()
	timer = get_node("timer")
	
	timer.connect("timeout", self, "tick")
	
	tick()


# timer tick
func tick():
	timer.start(tick_interval)
	check_target_distance()
	locate()


# determine if a target is located
func has_target():
	if(target):
		return target.has_ref()
	else:
		return false


# return the target
func get_target():
	return target.get_ref()


# get the distance to a target
func get_distance():
	if(has_target()):
		var dist = target.get_ref().global_position.distance_to(actor.global_position)
		return dist
	return INF


# get the direction/bearing to the target
func get_bearing():
	if(has_target()):
		return (target.get_ref().global_position - actor.global_position).normalized()
	else:
		return Vector2.ZERO


# check to see if the current target exceeds maximum distance
func check_target_distance():
	if(has_target()):
		if(get_distance() > ignore_range):
			target = null
			emit_signal("target_lost")


# locate a target
func locate():
	var objects_of_tag = get_tree().get_nodes_in_group(locate_by_tag)
	
	var min_dist : float = INF
	var min_dist_object
	
	for object in objects_of_tag:
		if(exclude_self and object == actor):
			continue
		
		var dist = object.global_position.distance_to(actor.global_position)
		if(dist < min_dist):
			min_dist = dist
			min_dist_object = object
	
	if(min_dist_object and min_dist <= detect_range):
		target = SafeRef.new(min_dist_object)
		emit_signal("target_found")
