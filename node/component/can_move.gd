extends Node


# class
class_name CanMove


# config
export var ground_speed : float = 50
export var ground_accel : float = 200
export var air_speed : float = 30
export var air_accel : float = 100


# ref
var actor : Actor


# var
var bearing : Vector2


# init
func _ready():
	actor = get_parent()


# physics
func _physics_process(delta):
	var speed = air_speed
	var accel = air_accel
	
	if(actor.grounded):
		speed = ground_speed
		accel = ground_accel
	
	actor.push_until(bearing * accel, bearing * speed, delta)


# perform run or airdrift
func move(new_bearing):
	bearing = sign(new_bearing.x) * Vector2.RIGHT


# stop movement
func stop():
	bearing = Vector2.ZERO
