extends Node


# class
class_name CanResistMagic


# config
export(Hitbox.tags) var magic_type = Hitbox.tags.GREEN_MAGIC


# ref
var hurtbox : Hitbox


# init
func _ready():
	hurtbox = get_parent().get_node("hurtbox")
	
	hurtbox.connect("hitbox_entered", self, "on_hitbox_entered")


# handle magic
func on_hitbox_entered(hitbox):
	if(hitbox.tag == magic_type):
		hitbox.successful_hit()
