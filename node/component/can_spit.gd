extends Ability


# class
class_name CanSpit


# config
export var charge_time : float = 0.50
export var spit_time : float = 0.50
export var cooldown_time : float = 1.0
export var offset : Vector2
export var angle : float = 70


# states
enum states {
	IDLE
	CHARGING
	READY
	SPITTING
	COOLDOWN
}


# when charge timer is finished
func on_timeout():
	match(state):
		
		states.CHARGING:
			change_state(states.READY)
	
		states.SPITTING:
			change_state(states.COOLDOWN)
			timer.start(cooldown_time)
	
		states.COOLDOWN:
			change_state(states.IDLE)


# initiates a spit attack
func charge_spit():
	if(state == states.IDLE):
		change_state(states.CHARGING)
		timer.start(charge_time)


# enacts a spit, which can only happen when it's ready
func spit(bearing):
	if(state == states.READY):
		change_state(states.SPITTING)
		timer.start(spit_time)
		
		bearing = (bearing * Vector2.RIGHT).normalized()
		
		var fireball = Creator.create_at_position("projectile/pumpkid_fireball", actor.global_position + offset)
		
		if(bearing.x > 0):
			bearing = bearing.rotated(deg2rad(-angle))
		else:
			bearing = bearing.rotated(deg2rad(angle))
		
		fireball.launch(bearing)


# determine if the actor can spit
func can_spit():
	return state == states.IDLE and actor.grounded
