extends Node2D


# class
class_name HasHealth


# config
export var max_health : int = 10
export var low_percentage : float = 0.40
export var display_time : float = 2.5
export var hurtbox_name : String = "hurtbox"
export(Array, Hitbox.tags) var hurt_by


# ref
var actor : Actor
var hurtbox : Hitbox
var timer : Timer
var label : Label
var animator : AnimationPlayer


# signal
signal death


# var
var health : int


# init
func _ready():
	actor = get_parent()
	hurtbox = actor.get_node(hurtbox_name)
	timer = get_node("timer")
	label = get_node("color_rect/label")
	animator = get_node("animator")
	
	set_health(INF)
	
	hurtbox.connect("hitbox_entered", self, "on_hitbox_entered")
	timer.connect("timeout", self, "on_timeout")
	
	visible = false


# handle a hitbox
func on_hitbox_entered(hitbox):
	if(hitbox.tag in hurt_by):
		hitbox.successful_hit()
		hurt(hitbox.damage)
		perform_knockback(hitbox)


# hide display when timer runs out
func on_timeout():
	visible = false


# show health meter
func display_health():
	timer.stop()
	timer.start(display_time)
	visible = true


# set health with clamping
func set_health(new_health):
	health = clamp(new_health, 0, max_health)
	label.text = str(health)
	
	if(health <= max_health * low_percentage):
		animator.play("hurt_red")
	else:
		animator.play("hurt_white")


# take damage and apply velocity impulse
func hurt(damage):
	if(not is_dead() and damage > 0):
		set_health(health - damage)
		var dmg_number = Creator.create_at_position("node/damage_number/damage_number", actor.global_position)
		dmg_number.set_damage(damage)
		display_health()
		
		if(is_dead()):
			emit_signal("death")
			actor.queue_free()


# add health / heal
func heal(heal_amount):
	if(is_dead() or heal_amount <= 0):
		return
	
	set_health(health + heal_amount)
	display_health()


# calculate knockback, based on position and angle
func perform_knockback(hitbox):
	var angle_rads = deg2rad(hitbox.knockback_angle) * -1
	var knockback = (Vector2.RIGHT * hitbox.knockback_speed).rotated(angle_rads)
	
	if(not hitbox.knockback_angle_absolute and hitbox.global_position.x > actor.global_position.x):
		knockback *= Vector2(-1, 1)
	
	if(hitbox.knockback_stop):
		actor.set_vel(Vector2.ZERO)
	
	actor.add_vel(knockback)
	actor.face_towards(knockback * Vector2.LEFT)


# determine if dead
func is_dead():
	return health <= 0


# determine if actor is at full health
func is_full():
	return health == max_health
