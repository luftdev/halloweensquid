extends Node


# class
class_name HasInk


# config
export var max_ink : int = 3


# ref
var actor : Actor
var ticker : Ticker
var ink_meter : SafeRef


# signal
signal ink_change


# var
var ink : int


# init
func _ready():
	actor = get_parent()
	ticker = get_node("ticker")
	ink_meter = SafeRef.new( ScreenManager.load_screen("ink_meter") )
	
	ink_meter.get_ref().resize_meter(max_ink)
	
	set_ink(INF)
	
	ticker.connect("tick", self, "on_tick")


# set the ink amount
func set_ink(new_ink):
	ink = clamp(new_ink, 0, max_ink)
	if(ink_meter.has_ref()):
		ink_meter.get_ref().set_ink(ink)
	
	emit_signal("ink_change", ink)


# add more ink
func add_ink(amount):
	set_ink(ink + amount)


# use up ink
func subtract_ink(amount):
	set_ink(ink - amount)


# handle tick
func on_tick():
	if(actor.grounded):
		set_ink(ink + 1)


# determine if there is enough ink for an inkshot
func has_ink():
	return ink > 0
