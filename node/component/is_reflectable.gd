extends Node


# class
class_name IsReflectable


# config
export var path_name : String = "path_straight"
export var become_hazard : bool = true
export(Array, Hitbox.tags) var reflected_by = [Hitbox.tags.INK]


# ref
var projectile : Projectile
var hurtbox : Hitbox
var hitbox : Hitbox
var reflect_path : ProjectilePath


# var
var is_reflected : bool = false


# init
func _ready():
	projectile = get_parent()
	hurtbox = projectile.get_node("hurtbox")
	hitbox = projectile.get_node("hitbox")
	reflect_path = projectile.get_node(path_name)
	
	hurtbox.connect("hitbox_entered", self, "on_hitbox_entered")


# handle redirect from ink
func on_hitbox_entered(hitbox):
	if(hitbox.tag in reflected_by):
		var bearing = Vector2.RIGHT.rotated(deg2rad(-hitbox.knockback_angle))
		
		if(abs(bearing.x) > abs(bearing.y)):
			bearing = sign(bearing.x) * Vector2.RIGHT
		else:
			bearing = sign(bearing.y) * Vector2.DOWN
		
		if(not hitbox.knockback_angle_absolute and hitbox.global_position.x > projectile.global_position.x):
			bearing *= Vector2(-1, 1)
		
		var reflect_location = hitbox.global_position + (projectile.global_position - hitbox.global_position).project(bearing)
		
		projectile.global_position = lerp(reflect_location, projectile.global_position, 0.5)
		
		reflect(bearing)


# change direction
func reflect(bearing):
	projectile.projectile_path = reflect_path
	
	if(become_hazard):
		hitbox.tag = Hitbox.tags.HAZARD
	
	if(hitbox.enabled and not is_reflected):
		hitbox.disable()
		hitbox.enable()
		is_reflected = true
	
	projectile.launch(bearing)
