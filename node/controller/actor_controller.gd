extends Node


# class
class_name ActorController, "res://node/controller/icon_controller.png"


# ref
var actor : Actor


# signal
signal stick_pressed
signal stick_released
signal stick_held
signal jump_pressed
signal jump_released
signal jump_held
signal action_pressed
signal action_released
signal action_held


# var
var stick : Vector2
var jump : bool
var action : bool


# init
func _ready():
	actor = get_parent()


# check if stick is held
func stick_held():
	return stick != Vector2.ZERO


# set the stick
func set_stick(new_stick):
	var old_stick = stick
	stick = new_stick
	
	if(old_stick == Vector2.ZERO and stick != Vector2.ZERO):
		emit_signal("stick_pressed", stick)
	elif(old_stick != Vector2.ZERO and stick == Vector2.ZERO):
		emit_signal("stick_released")
	
	if(stick != Vector2.ZERO):
		emit_signal("stick_held", stick)


# set jump
func set_jump(new_jump):
	var old_jump = jump
	jump = new_jump
	
	if(jump and not old_jump):
		emit_signal("jump_pressed")
	elif(old_jump and not jump):
		emit_signal("jump_released")
	
	if(jump):
		emit_signal("jump_held")


# set jump
func set_action(new_action):
	var old_action = action
	action = new_action
	
	if(action and not old_action):
		emit_signal("action_pressed")
	elif(old_action and not action):
		emit_signal("action_released")
	
	if(action):
		emit_signal("action_held")
