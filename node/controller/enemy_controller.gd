extends ActorController


# class
class_name EnemyController


# ref
var can_locate : CanLocate


# init
func _ready():
	can_locate = actor.get_node("can_locate")


# logic for locating the player
func locate_player():
	if(can_locate.has_target()):
		return BehaviorNode.status.SUCCESS
	else:
		return BehaviorNode.status.FAILURE


# determine whether or not the player is close enough to attack
func target_within_distance(dist):
	if(can_locate.has_target()):
		if(can_locate.get_distance() <= dist):
			return BehaviorNode.status.SUCCESS
	return BehaviorNode.status.FAILURE
