extends ActorController


# class
class_name PlayerController


# frame
func _process(delta):
	# get directional input
	var stick_x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	var stick_y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	var new_stick = Vector2(stick_x, stick_y)
	
	# set stick and fire events
	set_stick(new_stick)
	
	# set jump & action and fire events
	set_jump(Input.get_action_strength("in_jump") > 0)
	set_action(Input.get_action_strength("in_action") > 0)
