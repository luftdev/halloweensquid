extends EnemyController


# class
class_name PumpkidController


# ref
var can_headbonk : CanHeadbonk
var can_spit : CanSpit


# init
func _ready():
	if(actor.has_node("can_headbonk")):
		can_headbonk = actor.get_node("can_headbonk")
	
	if(actor.has_node("can_spit")):
		can_spit = actor.get_node("can_spit")


# 1-frame jump, but holds button down for a full jump
func single_jump():
	set_jump(false)
	set_jump(true)


# determine if headbonk attack is ready
func can_headbonk():
	if(can_headbonk and can_headbonk.can_headbonk()):
		return BehaviorNode.status.SUCCESS
	return BehaviorNode.status.FAILURE


# determine if spit attack is ready
func can_spit():
	if(can_spit and can_spit.can_spit()):
		return BehaviorNode.status.SUCCESS
	return BehaviorNode.status.FAILURE


# logic for running towards the player
func run_towards_player():
	if(not can_locate.has_target()):
		return BehaviorNode.status.FAILURE
	
	set_stick(can_locate.get_bearing())
	return BehaviorNode.status.SUCCESS


# determines if the player is above pumpkid
func below_player():
	if(not can_locate.has_target()):
		return BehaviorNode.status.FAILURE
	
	var bearing = can_locate.get_bearing()
	
	if(bearing.y < 0 and abs(bearing.y) > abs(bearing.x)):
		return BehaviorNode.status.SUCCESS
	else:
		return BehaviorNode.status.FAILURE


# if bumping into a wall
func bumping_wall():
	if(actor.wall_left or actor.wall_right):
		return BehaviorNode.status.SUCCESS
	else:
		return BehaviorNode.status.FAILURE
