extends EnemyController


# class
class_name DisceyepleController


# ref
var can_cast : CanCast


# init
func _ready():
	can_cast = actor.get_node("can_cast")


# determine if the disceyeple is able to cast magic
func can_cast():
	if(can_cast and can_cast.can_cast()):
		return BehaviorNode.status.SUCCESS
	else:
		return BehaviorNode.status.FAILURE


# make the disceyeple face the player
func face_target():
	if(can_locate and can_locate.has_target()):
		var new_stick = (can_locate.get_bearing() * Vector2.RIGHT).normalized()
		set_stick(new_stick)
		return BehaviorNode.status.SUCCESS
	else:
		return BehaviorNode.status.FAILURE


