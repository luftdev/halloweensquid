extends Node2D


# class
class_name DamageNumber


# change the numeric value on the label
func set_damage(dmg):
	var label = get_node("transform/label")
	label.text = "-" + str(abs(dmg))
