extends Timer


# class
class_name DespawnTimer


# init
func _ready():
	connect("timeout", get_parent(), "queue_free")
	
	process_mode = TIMER_PROCESS_PHYSICS
	
	start()
