extends Actor


# class
class_name Gate


# config
export var channel : int


# init
func _ready():
	ChannelManager.connect_change(channel, self, "on_channel_change")
	set_physics_process(false)


# update when channel changes
func on_channel_change(value):
	if(value):
		disable_collision()
		sprite.hide()
	else:
		enable_collision()
		sprite.show()
