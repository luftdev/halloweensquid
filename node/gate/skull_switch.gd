extends Actor


# class
class_name SkullSwitch


# config
export var channel : int
export var init_value : bool
export var lock_after_switch : bool


# ref
var hurtbox : Hitbox

# var
var is_on : bool
var is_locked : bool


# init
func _ready():
	hurtbox = get_node("hurtbox")
	
	set_switch(init_value)
	
	set_physics_process(false)
	
	hurtbox.connect("hitbox_entered", self, "on_hitbox_entered")


# handle being struck
func on_hitbox_entered(hitbox):
	if(is_locked):
		return
	
	if(not animator.is_playing()):
		set_switch(not is_on)
		if(lock_after_switch):
			is_locked = true
	
	hitbox.successful_hit()


# set switch state
func set_switch(value):
	is_on = value
	ChannelManager.write(channel, value)
	
	if(is_on):
		animator.play("on")
	else:
		animator.play('off')
