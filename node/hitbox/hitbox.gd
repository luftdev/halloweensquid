
extends Area2D


# class
class_name Hitbox, "res://node/hitbox/icon_hitbox.png"


# enum
enum types {
	HITBOX
	HURTBOX
}
enum tags {
	HAZARD
	PLAYER
	PLAYER_ATTACK
	ENEMY
	ENEMY_ATTACK
	INK
	PICKUP
	GOAL
	GREEN_MAGIC
	RED_MAGIC
	GREEN_TORCH
	RED_TORCH
}


# config
export(types) var type
export var draw_rect = false
export var enabled = true
export(tags) var tag
export var damage : int = 1
export var knockback_speed : float = 100
export var knockback_angle : float = 60
export var knockback_stop : bool = true
export var knockback_angle_absolute : bool = false


# signals
signal hitbox_entered
signal hurtbox_entered
signal successful_hit


# init
func _ready():
	# add to group
	add_to_group("hitbox")
	
	# hide color rect
	if(not draw_rect and not Debug.visible_hitboxes):
		hide()
	
	# set rect color
	if(type == types.HURTBOX):
		get_node("color_rect").color = Color(1, 1, 0, 0.5)
	
	# detect hitbox overlaps on ready
	if(enabled):
		enable()
	
	# signal
	connect("area_entered", self, "on_area_entered")


# enable interactions
func enable():
	enabled = true
	for area in get_overlapping_areas():
		on_area_entered(area)
		area.on_area_entered(self)


# disable interactions
func disable():
	enabled = false


# make visible
func unhide():
	get_node("color_rect").visible = true


# make invisible
func hide():
	get_node("color_rect").visible = false


# change the tag of hitbox
func change_tag(new_tag):
	if(new_tag != tag):
		tag = new_tag
		
		if(enabled):
			disable()
			enable()


# use to trigger a successful hit
func successful_hit():
	emit_signal("successful_hit")


# handle area2d collisions and find hitboxes
func on_area_entered(area):
	if(not area.is_in_group("hitbox")):
		return
	
	if(type == area.type):
		return
	
	if(not (enabled and area.enabled)):
		return
	
	if(type == types.HURTBOX):
		call_deferred("emit_signal", "hitbox_entered", area)
	else:
		call_deferred("emit_signal", "hurtbox_entered", area)
