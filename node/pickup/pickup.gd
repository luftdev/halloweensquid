extends Actor


# class
class_name Pickup


# config
export var destroy_on_successful_pickup : bool = true


# var
var is_picked_up : bool = false


# attempt a pickup if possible
func attempt_pickup(actor : Actor):
	if(is_picked_up):
		return false
	
	on_pickup(actor)
	
	return is_picked_up


# declare this pickup as having been successfully picked up
func succeed_pickup():
	is_picked_up = true
	
	if(destroy_on_successful_pickup):
		call_deferred("queue_free")


# perform actions on pickup
func on_pickup(actor : Actor):
	pass
