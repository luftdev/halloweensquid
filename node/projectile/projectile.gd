extends Actor


# class
class_name Projectile


# config
export var launch_speed : float = 100


# ref
var hitbox : Hitbox
var projectile_path : ProjectilePath


# var
var bearing : Vector2


# init
func _ready():
	hitbox = get_node("hitbox")
	
	connect("collision", self, "on_collision")
	hitbox.connect("successful_hit", self, "on_successful_hit")


# limit a vector to one of four directions
func vector_four_direction(vec):
	if(abs(vec.x) > (vec.y)):
		return sign(vec.x) * Vector2.RIGHT
	else:
		return sign(vec.y) * Vector2.DOWN


# launch in a direction
func launch(new_bearing):
	bearing = new_bearing.normalized()
	
	if(projectile_path):
		projectile_path.launch(bearing)


# handle basic collision event
func on_collision():
	if(projectile_path):
		projectile_path.on_collision()


# handle successful hits
func on_successful_hit():
	projectile_path.on_successful_hit()


# frame
func _process(delta):
	if(projectile_path):
		projectile_path.on_process(delta)


# physics
func _physics_process(delta):
	if(projectile_path):
		projectile_path.on_physics_process(delta)
