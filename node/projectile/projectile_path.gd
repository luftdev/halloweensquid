extends Node


# class
class_name ProjectilePath, "res://node/projectile/icon_projectile_path.png"


# ref
var projectile


# the first projectile path in the tree will be the default assigned
func _ready():
	projectile = get_parent()
	
	if(not get_parent().projectile_path):
		get_parent().projectile_path = self


# launch and set flight direction
func launch(bearing):
	pass


# handle collisions
func on_collision():
	projectile.queue_free()


# destory on successful hit
func on_successful_hit():
	projectile.queue_free()


# frame
func on_process(delta):
	pass


# physics
func on_physics_process(delta):
	pass
