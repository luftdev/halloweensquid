extends Reference


# class
class_name SafeRef


# ref
var ref


# signal
signal broken


# init
func _init(var node):
	._init()
	make_reference(node)


# determine if safe
func has_ref():
	return (ref != null)


# get reference object
func get_ref():
	return ref


# make reference
func make_reference(node):
	ref = node
	ref.connect("tree_exiting", self, "break_reference")


# break reference
func break_reference():
	ref.disconnect("tree_exiting", self, "break_reference")
	ref = null
	emit_signal("broken", self)
