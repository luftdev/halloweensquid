extends Node


# class
class_name State


# ref
var state_machine : StateMachine
var actor : Actor


# init
func _ready():
	state_machine = get_parent()
	actor = state_machine.get_parent()


# perform starting operations
func on_enter():
	print("entered ", name)


# perform leaving operations, like cleanup
func on_leave():
	print("left ", name)


# frame
func on_process(delta):
	pass


# physics
func on_physics_process(delta):
	pass
