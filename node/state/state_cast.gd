extends State


# class
class_name StateCast


func on_enter():
	var can_cast = actor.get_node("can_cast")
	if(can_cast):
		can_cast.cast(state_machine.controller.stick)


func on_leave():
	pass
