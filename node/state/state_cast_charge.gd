extends State


# class
class_name StateCastCharge


func on_enter():
	actor.face_towards(state_machine.controller.stick)
	
	var can_cast = actor.get_node("can_cast")
	if(can_cast):
		can_cast.charge_cast()


func on_leave():
	pass
