extends State


func on_enter():
	var can_headbonk = actor.get_node("can_headbonk")
	var bearing = state_machine.controller.stick
	can_headbonk.headbonk(bearing)


func on_leave():
	var can_move = actor.get_node("can_move")
	can_move.stop()
