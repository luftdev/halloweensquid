extends State


# class
class_name StateHeadbonkCharge


func on_enter():
	var can_headbonk = actor.get_node("can_headbonk")
	var can_move = actor.get_node("can_move")
	can_headbonk.charge_headbonk()
	can_move.stop()


func on_leave():
	pass
