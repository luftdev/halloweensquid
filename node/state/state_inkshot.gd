extends State


# class
class_name StateInkshot


# ref
var can_inkshot
var has_ink


func on_enter():
	if(not can_inkshot):
		can_inkshot = actor.get_node("can_inkshot")
	
	if(not has_ink):
		has_ink = actor.get_node("has_ink")
	
	var bearing = state_machine.controller.stick
	bearing = can_inkshot.set_bearing(bearing)
	can_inkshot.inkshot(bearing)
	
	has_ink.subtract_ink(1)
	
	actor.sprite.rotation = bearing.angle() + PI * 0.5


func on_leave():
	actor.sprite.rotation = 0
