extends State


# class
class_name StateInkshotCharge


# ref
var can_inkshot
var can_blink


func on_enter():
	if(not can_inkshot):
		can_inkshot = actor.get_node("can_inkshot")
	
	if(not can_blink):
		can_blink = actor.get_node("can_blink")
	
	can_blink.blink(Color.orange, 0.06)


func on_leave():
	can_blink.stop()


func on_process(delta):
	var bearing = state_machine.controller.stick
	bearing = can_inkshot.set_bearing(bearing)
	actor.sprite.rotation = bearing.angle() + PI * 0.5
