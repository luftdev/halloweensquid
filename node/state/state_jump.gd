extends State


# class
class_name StateJump


# ref
var can_move
var can_jump


func on_enter():
	if(not can_move):
		can_move = actor.get_node("can_move")
	
	if(not can_jump):
		can_jump = actor.get_node("can_jump")
	
	can_jump.jump()


func on_leave():
	can_move.stop()


func on_process(delta):
	actor.face_towards(state_machine.controller.stick)


func on_physics_process(delta):
	can_move.move(state_machine.controller.stick)
	
	if(not state_machine.controller.jump):
		can_jump.shorten()
