extends State


# class
class_name StatePumpkidBurst


func on_enter():
	var can_move : CanMove = actor.get_node("can_move")
	if(can_move):
		can_move.stop()
	
	var actor_push : ActorPush = actor.get_node("actor_push")
	if(actor_push):
		actor_push.disable()
	
	var hurtbox : Hitbox = actor.get_node("hurtbox")
	if(hurtbox):
		hurtbox.disable()
	
	var hitbox : Hitbox = actor.get_node("hitbox")
	if(hitbox):
		hitbox.disable()
	
	var plume = Creator.create_at_position("projectile/pumpkid_plume", actor.global_position)
	plume.launch(Vector2.UP)


func on_leave():
	var actor_push : ActorPush = actor.get_node("actor_push")
	if(actor_push):
		actor_push.enable()
	
	var hurtbox : Hitbox = actor.get_node("hurtbox")
	if(hurtbox):
		hurtbox.enable()
