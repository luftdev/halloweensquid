extends State


# class
class_name StatePumpkidSpit


func on_enter():
	var can_spit = actor.get_node("can_spit")
	
	actor.face_towards(state_machine.controller.stick)
	
	can_spit.spit(state_machine.controller.stick)


func on_leave():
	pass
