extends State


# class
class_name StatePumpkidSpitCharge


func on_enter():
	var can_spit = actor.get_node("can_spit")
	var can_move = actor.get_node("can_move")
	can_spit.charge_spit()
	can_move.stop()


func on_leave():
	pass
