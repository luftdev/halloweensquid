extends State


# class
class_name StateRun


# ref
var can_move


func on_enter():
	if(not can_move):
		can_move = actor.get_node("can_move")


func on_leave():
	can_move.stop()


func on_process(delta):
	actor.face_towards(state_machine.controller.stick)


func on_physics_process(delta):
	can_move.move(state_machine.controller.stick)
