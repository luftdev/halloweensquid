extends StateMachine


# class
class_name PumpkidSpitterStateMachine


# ref
var can_jump
var can_spit


# state id
enum state_id {
	PLANTED
	IDLE
	RUN
	JUMP
	FALL
	SPIT_CHARGE
	SPIT
}


# init
func _ready():
	state_id.IDLE = get_id_by_name("state_idle")
	state_id.PLANTED = get_id_by_name("state_planted")
	state_id.RUN = get_id_by_name("state_run")
	state_id.JUMP = get_id_by_name("state_jump")
	state_id.FALL = get_id_by_name("state_fall")
	state_id.SPIT_CHARGE = get_id_by_name("state_pumpkid_spit_charge")
	state_id.SPIT = get_id_by_name("state_pumpkid_spit")
	can_jump = actor.get_node("can_jump")
	can_spit = actor.get_node("can_spit")
	
	can_jump.connect("peak", self, "on_peak")
	can_spit.connect("state_change", self, "on_spit_state_change")


func on_land():
	
	match current_state_id:
		
		state_id.FALL, state_id.JUMP:
			if(abs(controller.stick.x) > 0):
				change_state(state_id.RUN)
			else:
				change_state(state_id.IDLE)


func on_slide_off():
	
	match current_state_id:
		
		state_id.IDLE, state_id.RUN:
			change_state(state_id.FALL)


func on_stick_pressed(stick):
	
	match current_state_id:
		
		state_id.PLANTED:
			change_state(state_id.JUMP)


func on_stick_released():
	
	match current_state_id:
		
		state_id.RUN:
			change_state(state_id.IDLE)


func on_jump_pressed():
	
	match current_state_id:
		
		state_id.PLANTED:
			change_state(state_id.JUMP)
		
		state_id.IDLE, state_id.RUN:
			if(actor.grounded):
				change_state(state_id.JUMP)


func on_action_pressed():
	
	match current_state_id:
		
		state_id.PLANTED:
			change_state(state_id.JUMP)
		
		state_id.IDLE, state_id.RUN:
			if(can_spit and can_spit.can_spit()):
				change_state(state_id.SPIT_CHARGE)


func on_peak():
	
	match current_state_id:
		
		state_id.JUMP:
			if(actor.grounded):
				change_state(state_id.IDLE)
			else:
				change_state(state_id.FALL)


func on_spit_state_change(ability_state):
	
	match ability_state:
		
		CanSpit.states.READY:
			if(current_state_id == state_id.SPIT_CHARGE):
				change_state(state_id.SPIT)
		
		CanSpit.states.COOLDOWN:
			change_state(state_id.IDLE)
