extends StateMachine


# class
class_name PumpkidStateMachine


# ref
var can_jump
var can_headbonk


# state id
enum state_id {
	PLANTED
	IDLE
	RUN
	JUMP
	FALL
	HEADBONK_CHARGE
	HEADBONK
	BURST
}


# init
func _ready():
	state_id.IDLE = get_id_by_name("state_idle")
	state_id.PLANTED = get_id_by_name("state_planted")
	state_id.RUN = get_id_by_name("state_run")
	state_id.JUMP = get_id_by_name("state_jump")
	state_id.FALL = get_id_by_name("state_fall")
	state_id.HEADBONK_CHARGE = get_id_by_name("state_headbonk_charge")
	state_id.HEADBONK = get_id_by_name("state_headbonk")
	state_id.BURST = get_id_by_name("state_pumpkid_burst")
	can_jump = actor.get_node("can_jump")
	can_headbonk = actor.get_node("can_headbonk")
	
	can_jump.connect("peak", self, "on_peak")
	can_headbonk.connect("state_change", self, "on_headbonk_state_change")


func on_land():
	
	match current_state_id:
		
		state_id.FALL, state_id.JUMP:
			if(abs(controller.stick.x) > 0):
				change_state(state_id.RUN)
			else:
				change_state(state_id.IDLE)


func on_slide_off():
	
	match current_state_id:
		
		state_id.IDLE, state_id.RUN:
			change_state(state_id.FALL)


func on_stick_pressed(stick):
	
	match current_state_id:
		
		state_id.PLANTED:
			change_state(state_id.JUMP)
		
		state_id.IDLE:
			if(actor.grounded and controller.stick.x != 0):
				change_state(state_id.RUN)


func on_stick_released():
	
	match current_state_id:
		
		state_id.RUN:
			change_state(state_id.IDLE)


func on_jump_pressed():
	
	match current_state_id:
		
		state_id.PLANTED:
			change_state(state_id.JUMP)
		
		state_id.IDLE, state_id.RUN:
			if(actor.grounded):
				change_state(state_id.JUMP)


func on_action_pressed():
	
	match current_state_id:
		
		state_id.PLANTED:
			change_state(state_id.JUMP)
		
		state_id.IDLE, state_id.RUN:
			if(can_headbonk.can_headbonk()):
				change_state(state_id.HEADBONK_CHARGE)


func on_peak():
	
	match current_state_id:
		
		state_id.JUMP:
			if(actor.grounded):
				change_state(state_id.IDLE)
			else:
				change_state(state_id.FALL)


func on_headbonk_state_change(ability_state):
	
	match ability_state:
		
		CanHeadbonk.states.READY:
			if(current_state_id == state_id.HEADBONK_CHARGE):
				change_state(state_id.HEADBONK)
		
		CanHeadbonk.states.COOLDOWN:
			change_state(state_id.BURST)
