extends StateMachine


# class
class_name SquidStateMachine


# ref
var can_jump
var can_inkshot
var has_ink


# state id
enum state_id {
	IDLE
	RUN
	JUMP
	FALL
	INKSHOT
	INKSHOT_CHARGE
}


# init
func _ready():
	state_id.IDLE = get_id_by_name("state_idle")
	state_id.RUN = get_id_by_name("state_run")
	state_id.JUMP = get_id_by_name("state_jump")
	state_id.FALL = get_id_by_name("state_fall")
	state_id.INKSHOT = get_id_by_name("state_inkshot")
	state_id.INKSHOT_CHARGE = get_id_by_name("state_inkshot_charge")
	can_jump = actor.get_node("can_jump")
	can_inkshot = actor.get_node("can_inkshot")
	has_ink = actor.get_node("has_ink")
	
	can_jump.connect("peak", self, "on_peak")
	can_inkshot.connect("state_change", self, "on_inkshot_state_change")
	has_ink.connect("ink_change", self, "on_ink_change")


func on_land():
	
	match current_state_id:
		
		state_id.FALL, state_id.JUMP:
			if(abs(controller.stick.x) > 0):
				change_state(state_id.RUN)
			else:
				change_state(state_id.IDLE)


func on_slide_off():
	
	match current_state_id:
		
		state_id.IDLE, state_id.RUN:
			change_state(state_id.FALL)


func on_stick_pressed(stick):
	
	match current_state_id:
		
		state_id.IDLE:
			if(actor.grounded and controller.stick.x != 0):
				change_state(state_id.RUN)


func on_stick_held(stick):
	
	match current_state_id:
		
		state_id.IDLE:
			if(actor.grounded and controller.stick.x != 0):
				change_state(state_id.RUN)


func on_stick_released():
	
	match current_state_id:
		
		state_id.RUN:
			change_state(state_id.IDLE)


func on_jump_pressed():
	
	match current_state_id:
		
		state_id.IDLE, state_id.RUN:
			if(actor.grounded):
				change_state(state_id.JUMP)


func on_action_pressed():
	
	match current_state_id:
		
		state_id.IDLE, state_id.RUN, state_id.JUMP, state_id.FALL:
			if(can_inkshot.can_inkshot() and has_ink.has_ink()):
				change_state(state_id.INKSHOT_CHARGE)


func on_action_released():
	
	match current_state_id:
		
		state_id.INKSHOT_CHARGE:
			if(can_inkshot.can_inkshot()):
				change_state(state_id.INKSHOT)


func on_peak():
	
	match current_state_id:
		
		state_id.JUMP:
			if(actor.grounded):
				change_state(state_id.IDLE)
			else:
				change_state(state_id.FALL)


func on_inkshot_state_change(ability_state):
	
	if(ability_state == CanInkshot.states.IDLE and current_state_id == state_id.INKSHOT):
		
		if(controller.action and has_ink.has_ink() and can_inkshot.can_inkshot()):
			change_state(state_id.INKSHOT_CHARGE)
		
		elif(actor.grounded):
			if(abs(controller.stick.x) > 0):
				change_state(state_id.RUN)
			else:
				change_state(state_id.IDLE)
		else:
			change_state(state_id.FALL)


func on_ink_change(ink):
	if(ink > 0 and controller.action and can_inkshot.can_inkshot()):
		change_state(state_id.INKSHOT_CHARGE)
