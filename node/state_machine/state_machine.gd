extends Node


# class
class_name StateMachine, "res://node/state_machine/icon_state_machine.png"


# config
export var controller_name : String = "actor_controller"


# ref
var states : Array
var actor : Actor
var controller : ActorController


# signal
signal state_change


# var
var current_state
var current_state_id : int


# init
func _ready():
	states = get_children()
	actor = get_parent()
	controller = actor.get_node(controller_name)
	call_deferred("change_state", 0)
	
	actor.connect("land", self, "on_land")
	actor.connect("slide_off", self, "on_slide_off")
	controller.connect("stick_pressed", self, "on_stick_pressed")
	controller.connect("stick_released", self, "on_stick_released")
	controller.connect("stick_held", self, "on_stick_held")
	controller.connect("jump_pressed", self, "on_jump_pressed")
	controller.connect("jump_released", self, "on_jump_released")
	controller.connect("jump_held", self, "on_jump_held")
	controller.connect("action_pressed", self, "on_action_pressed")
	controller.connect("action_released", self, "on_action_released")
	controller.connect("action_held", self, "on_action_held")


# get id of state
func get_id_by_name(state_name):
	var state = get_node(state_name)
	return states.find(state)


# perform when actor is landing on the ground
func on_land():
	pass


# perform when sliding off of a ledge
func on_slide_off():
	pass


# perform when receiving controller directional input
func on_stick_pressed(stick):
	pass


# perform when receiving controller directional input
func on_stick_released():
	pass


# perform while controller directional input is non-zero
func on_stick_held(stick):
	pass


# perform when receiving controller jump input
func on_jump_pressed():
	pass


# perform when receiving controller jump input
func on_jump_released():
	pass


# perform when receiving continuous controller jump input
func on_jump_held():
	pass


# used for attacks or special abilities
func on_action_pressed():
	pass


# used for attacks or special abilities
func on_action_released():
	pass


# perform when receiving continuous controller action input
func on_action_held():
	pass


# frame
func _process(delta):
	if(current_state):
		current_state.on_process(delta)


# physics
func _physics_process(delta):
	if(current_state):
		current_state.on_physics_process(delta)


# change from one state to another
func change_state(next_state_id):
	var next_state = states[next_state_id]
	
	if(not next_state):
		breakpoint
		return
	
	if(current_state):
		current_state.on_leave()
	
	
	current_state = next_state
	current_state_id = next_state_id
	next_state.on_enter()
	
	emit_signal("state_change", next_state.name)
