extends StateMachine


# class
class_name DisceyepleStateMachine


# state id
enum state_id {
	IDLE
	CHARGING
	CAST
}


# ref
var can_cast : CanCast


# init
func _ready():
	state_id.IDLE = get_id_by_name("state_idle")
	state_id.CHARGING = get_id_by_name("state_cast_charge")
	state_id.CAST = get_id_by_name("state_cast")
	can_cast = actor.get_node("can_cast")
	
	can_cast.connect("state_change", self, "on_cast_state_change")


func on_action_pressed():
	
	if(current_state_id == state_id.IDLE):
		change_state(state_id.CHARGING)


# handle casting state change
func on_cast_state_change(ability_state):
	
	match ability_state:
		
		CanCast.states.COOLDOWN:
			change_state(state_id.IDLE)
		
		CanCast.states.READY:
			change_state(state_id.CAST)
