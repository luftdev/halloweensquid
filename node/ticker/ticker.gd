extends Timer


# class
class_name Ticker


# signal
signal tick


# init
func _ready():
	connect("timeout", self, "on_timeout")
	start()


# perform a tick
func on_timeout():
	start()
	emit_signal("tick")
