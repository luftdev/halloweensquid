extends Pickup


# class
class_name HealthPickup


# config
export var heal_amount : int = 5


# pick up health only if the player isn't at full health
func on_pickup(actor : Actor):
	var has_health : HasHealth = actor.get_node("has_health")
	if(has_health and not has_health.is_full()):
		has_health.heal(heal_amount)
		succeed_pickup()
