extends ProjectilePath


# class
class_name PathArc


func launch(bearing):
	projectile.set_vel(bearing * projectile.launch_speed)
	projectile.face_towards(bearing)
