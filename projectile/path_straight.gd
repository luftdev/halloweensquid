extends ProjectilePath


# class
class_name PathStraight


# set velocity in a straight line and remove gravity influence
func launch(bearing):
	projectile.sprite.rotation = bearing.angle() + PI
	projectile.set_vel(bearing * projectile.launch_speed)
	projectile.disable_gravity()
	
	if(projectile.hitbox.knockback_angle_absolute):
		projectile.hitbox.knockback_angle = rad2deg(-bearing.angle())
