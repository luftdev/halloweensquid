extends Node


# filesystem
var root : String = "res://"
var scene_extension : String = ".tscn"


# var
var assets : Dictionary


# retrieve an asset on command
func get_asset(asset_path):
	if(asset_path in assets):
		return assets[asset_path]
	else:
		return load_asset(asset_path)


# load asset by path and cache it
func load_asset(asset_path):
	if(asset_path in assets):
		print(name, " tried loading \"%s\" twice" % asset_path)
		return null
	
	var full_asset_path = root + asset_path + scene_extension
	var asset = load(full_asset_path)
	
	if(not asset):
		print(name, " could not find \"%s\"" % asset_path)
		return null
	
	assets[asset_path] = asset
	return asset
