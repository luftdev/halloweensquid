extends Node


# path
var behavior_tree_path = "behavior_tree/"


# ref
var behavior_trees = {}


# request a behavior tree
func get_behavior_tree(behavior_tree_name):
	if(behavior_tree_name in behavior_trees):
		return behavior_trees[behavior_tree_name]
	else:
		return load_behvior_tree(behavior_tree_name)


# load a behavior tree
func load_behvior_tree(behavior_tree_name):
	var behavior_tree_asset = AssetTable.get_asset(behavior_tree_path + behavior_tree_name)
	var behavior_tree = Creator.create(behavior_tree_asset)
	
	# cache the behavior tree and add to the tree
	behavior_trees[behavior_tree_name] = behavior_tree
	behavior_tree.name = behavior_tree_name
	add_child(behavior_tree)
	
	return behavior_tree
