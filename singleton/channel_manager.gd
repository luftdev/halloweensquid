extends Node


# var
var channels = {}


# write true/false to a channel
func write(channel : int, value : bool):
	if(channel in channels):
		channels[channel].write(value)


# read current value of a channel
func read(channel : int):
	if(channel in channels):
		return channels[channel].value


# connect to a channel signal on write
func connect_write(channel : int, target, func_name : String):
	if(not channel in channels):
		channels[channel] = Channel.new()
	
	channels[channel].connect("channel_write", target, func_name)


# connect to a channel signal on change
func connect_change(channel : int, target, func_name : String):
	if(not channel in channels):
		channels[channel] = Channel.new()
	
	channels[channel].connect("channel_change", target, func_name)
