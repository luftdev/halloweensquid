extends Node


# create an instance of an asset
func create(asset):
	var instance = asset.instance()
	
	if(not instance):
		breakpoint
		return
	
	return instance


# use global position for position
func create_at_position(asset_path, position):
	var level = GameManager.get_game_scene()
	
	if(not level):
		return
	
	var asset = AssetTable.get_asset(asset_path)
	
	if(not asset):
		breakpoint
		return
	
	var instance = create(asset)
	
	if(not instance):
		breakpoint
		return
	
	level.add_child(instance)
	instance.name += str(randi())
	instance.global_position = position
	
	return instance
