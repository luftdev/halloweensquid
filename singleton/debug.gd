extends Node


# var
var visible_hitboxes : bool = false


# toggle visible hitboxes
func toggle_visible_hitboxes():
	if(visible_hitboxes):
		disable_visible_hitboxes()
	else:
		enable_visible_hitboxes()


# enable visible hitboxes
func enable_visible_hitboxes():
	visible_hitboxes = true
	
	for hitbox in get_tree().get_nodes_in_group("hitbox"):
		hitbox.unhide()


# enable visible hitboxes
func disable_visible_hitboxes():
	visible_hitboxes = false
	
	for hitbox in get_tree().get_nodes_in_group("hitbox"):
		hitbox.hide()
