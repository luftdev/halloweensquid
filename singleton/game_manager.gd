extends Node


# config
var first_scene = "test_scene"


# path
var master_path = "/root/master/"
var scene_path = "scene/"


# ref
var current_scene


# begin the game from start
func load_new_game():
	GameManager.load_scene(first_scene)


# load a game scene
func load_scene(scene_name):
	var previous_scene = end_scene()
	
	if(previous_scene):
		yield(previous_scene, "tree_exited")
	
	var scene_template = AssetTable.get_asset(scene_path + scene_name)
	var scene = Creator.create(scene_template)
	
	current_scene = scene
	
	var master_node = get_node(master_path)
	master_node.add_child(current_scene)


# frame
func _process(delta):
	if(current_scene):
		if(Input.is_action_just_pressed("in_pause")):
			ScreenManager.load_screen("pause_menu")


# remove game scene
func end_scene():
	if(current_scene):
		current_scene.queue_free()
		var previous_scene = current_scene
		current_scene = null
		return previous_scene


# get the current level node
func get_game_scene():
	return current_scene


# pause the game scene
func pause():
	get_tree().paused = true


# unpause the game scene
func unpause():
	get_tree().paused = false
