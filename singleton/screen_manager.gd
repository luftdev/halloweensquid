extends Node


# path
var ui_path = "/root/master/ui"
var screen_path = "ui/screen/"


# var
var current_screens : Dictionary


# check if screen is open by name
func is_open(screen_name):
	return screen_name in current_screens


# load a screen
func load_screen(screen_name):
	if(is_open(screen_name)):
		return get_screen(screen_name)
	
	var screen_template = AssetTable.get_asset(screen_path + screen_name)
	var screen = Creator.create(screen_template)
	
	if(not screen):
		return
	
	current_screens[screen_name] = screen
	
	var ui = get_node(ui_path)
	ui.add_child(screen)
	
	return screen


# access a screen object
func get_screen(screen_name):
	if(is_open(screen_name)):
		return current_screens[screen_name]
	return null


# remove a screen
func remove_screen(screen_name):
	if(is_open(screen_name)):
		current_screens[screen_name].queue_free()
		current_screens.erase(screen_name)


# remove a screen by value
func remove_screen_object(screen):
	for key in current_screens:
		if(current_screens[key] == screen):
			remove_screen(key)
			break
