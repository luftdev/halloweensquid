extends Screen


# ref
var border_white : ColorRect
var border_black : ColorRect
var meter : Node2D
var units : Array


# meter animation frames
enum frames {
	ICON
	FULL
	EMPTY
}


# var
var meter_value : int


# init
func _ready():
	border_white = get_node("border_white")
	border_black = get_node("border_black")
	meter = get_node("meter")
	
	resize_meter(1)


# resize the meter to the correct number of units
func resize_meter(size):
	# minimum size is 1
	size = max(1, size)
	
	# reduce number of units down to one
	for i in range(1, meter.get_child_count()):
		meter.get_child(i).queue_free()
	
	# duplicate necessary sprites
	var base_sprite = meter.get_child(0)
	
	units = [ base_sprite ]
	
	for i in range(1, size):
		# duplicate and displace
		var dupe = base_sprite.duplicate()
		dupe.position += Vector2.RIGHT * 8 * i
		
		# add to the meter
		meter.add_child(dupe)
		units.append(dupe)
	
	# the border has to adjust to accommodate the change in sprites
	resize_border()


# resize the border to match the number of units
func resize_border():
	var size = len(units)
	border_white.rect_min_size = Vector2(12 + size * 8, 12)
	border_black.rect_min_size = Vector2(10 + size * 8, 10)


# set the number of ink ticks
func set_ink(ink_amount):
	meter_value = ink_amount
	
	for i in range(len(units)):
		if(i < ink_amount):
			units[i].frame = frames.FULL
		else:
			units[i].frame = frames.EMPTY
