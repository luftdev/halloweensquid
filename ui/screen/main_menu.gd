extends Screen


# ref
var button_start : Button
var button_exit : Button


# init
func _ready():
	button_start = get_node("vbox/hbox/vbox/start")
	button_exit = get_node("vbox/hbox/vbox/exit")
	
	button_start.connect("pressed", self, "on_start_pressed")
	button_exit.connect("pressed", self, "on_exit_pressed")
	
	button_start.grab_focus()


# handle start button
func on_start_pressed():
	close_screen()
	GameManager.load_new_game()


# handle exit button
func on_exit_pressed():
	get_tree().quit()
