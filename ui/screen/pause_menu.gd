extends Screen


# ref
var resume : Button
var hitboxes : Button
var quit : Button


# init
func _ready():
	resume = get_node("center/vbox/resume")
	hitboxes = get_node("center/vbox/hitboxes")
	quit = get_node("center/vbox/quit")
	
	resume.grab_focus()
	GameManager.pause()
	
	resume.connect("pressed", self, "on_resume")
	hitboxes.connect("pressed", self, "on_hitboxes")
	quit.connect("pressed", self, "on_quit")


# cleanup
func _exit_tree():
	GameManager.unpause()


# frame
func _process(delta):
	if(Input.is_action_just_pressed("in_pause")):
		on_resume()


# resume gameplay
func on_resume():
	close_screen()


# toggle hitboxes
func on_hitboxes():
	Debug.toggle_visible_hitboxes()


# quit to main menu
func on_quit():
	GameManager.end_scene()
	ScreenManager.remove_screen("ink_meter")
	ScreenManager.load_screen("main_menu")
	close_screen()
